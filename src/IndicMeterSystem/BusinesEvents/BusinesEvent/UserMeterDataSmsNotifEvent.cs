﻿using System;

namespace BusinesEvent
{
    public class UserMeterDataSmsNotifEvent : UserSmsNotifEvent
    {
        private bool IsDisposed = false;

        public UserMeterDataSmsNotifEvent(string phoneNumber, DateTime eventDateTo,
            string address)
            :base(phoneNumber, eventDateTo)
        {
            Body = $"По адресу \"{address}\" отсутствуют показания на текущий месяц.";
        }

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
            }

            IsDisposed = true;
            base.Dispose(disposing);
        }
    }
}
