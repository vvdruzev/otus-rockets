﻿using System;

namespace BusinesEvent
{
    public class UserPaymentEmailNotifEvent : UserEmailNotifEvent
    {
        private bool IsDisposed = false;

        public UserPaymentEmailNotifEvent(string email, DateTime eventDateTo,
            string address, DateTime? date)
            : base(email, eventDateTo)
        {
            Title = "Истекает срок оплаты";
            Body = "Уважаемы пользователь!"
                    + Environment.NewLine
                    + $"По адресу \"{address}\" имеются неоплаченные показания, " +
                    $"внесенные датой: {date}.";
        }

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
            }

            IsDisposed = true;
            base.Dispose(disposing);
        }
    }
}
