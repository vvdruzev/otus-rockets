﻿using System;

namespace ClearAndTestInitDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            var initializer = new DbInitializer((new Settings(AppContext.BaseDirectory, "appsettings.json")).ConnectionString);
            initializer.InitDb();
            Console.ReadLine();
        }
    }
}
