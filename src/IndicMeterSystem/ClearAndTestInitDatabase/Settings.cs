﻿using Microsoft.Extensions.Configuration;

namespace ClearAndTestInitDatabase
{
    public class Settings
    {
        public string ConnectionString { get; private set; }
        public Settings(string basePath, string file)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile(file);
            IConfiguration config = builder.Build();

            var usedConStr = config["UsedConnection"];
            if (string.IsNullOrEmpty(usedConStr)) 
                usedConStr  = "DefaultConnection";
            ConnectionString = config.GetConnectionString(usedConStr);
        }

    }
}
