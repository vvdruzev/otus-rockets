﻿using System;
using System.Net;

using IndicMeterSystem.Common.Properties;

namespace IndicMeterSystem.Common.Exceptions
{
	/// <summary>
	/// Базовый класс для исключений в проекте.
	/// </summary>
	public abstract class AppException : Exception
	{
		/// <summary>
		/// Инициализирует новый экземпляр класса <see cref="AppException" />.
		/// </summary>
		/// <param name="errorCode">Код ошибки.</param>
		/// <param name="statusCode">HTTP status code.</param>
		/// <param name="innerException">Inner exception.</param>
		protected AppException(string errorCode, HttpStatusCode statusCode, Exception innerException = null)
			: base(GetErrorMessage(errorCode), innerException)
		{
			ErrorCode = errorCode;
			StatusCode = statusCode;
		}

		/// <summary>
		/// Инициализирует новый экземпляр класса <see cref="AppException" />.
		/// </summary>
		/// <param name="errorCode">Код ошибки.</param>
		/// <param name="message">Текст ошибки.</param>
		/// <param name="statusCode">HTTP status code.</param>
		/// <param name="innerException">Inner exception.</param>
		protected AppException(string errorCode, string message, HttpStatusCode statusCode, Exception innerException = null)
			: base(message, innerException)
		{
			ErrorCode = errorCode;
			StatusCode = statusCode;
		}

		/// <summary>
		/// HTTP status code.
		/// </summary>
		public HttpStatusCode StatusCode { get; }

		/// <summary>
		/// Error code.
		/// </summary>
		public string ErrorCode { get; }

		/// <summary>
		/// Возвращает текст ошибки по коду ошибки.
		/// </summary>
		/// <param name="errorCode">Код ошибки.</param>
		/// <returns>Текст ошибки.</returns>
		private static string GetErrorMessage(string errorCode)
			=> ErrorMessages.ResourceManager.GetString(errorCode, ErrorMessages.Culture);
	}
}
