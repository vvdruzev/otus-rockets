﻿using IndicMeterSystem.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace IndicMeterSystem.Data.Config
{
    class MeterConfig : IEntityTypeConfiguration<Meter>
    {
        public void Configure(EntityTypeBuilder<Meter> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.SerialNumber)
                .IsRequired();

            builder.Property(x => x.AddressId)
                .IsRequired();

            builder.Property(x => x.Description)
                .HasMaxLength(1000);

            builder.Property(x => x.DateFrom)
                .IsRequired();

            builder.HasOne(x => x.Address)
                .WithMany(y => y.Meters)
                .HasForeignKey(x => x.AddressId);

            builder.HasOne(x => x.Tariff)
                .WithMany()
                .HasForeignKey(x => x.TariffId);
        }
    }
}
