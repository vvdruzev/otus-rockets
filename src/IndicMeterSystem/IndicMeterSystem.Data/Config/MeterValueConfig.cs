﻿using IndicMeterSystem.Data.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IndicMeterSystem.Data.Config
{
    class MeterValueConfig : IEntityTypeConfiguration<MeterValue>
    {
        public void Configure(EntityTypeBuilder<MeterValue> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Number)
                .IsRequired();

            builder.Property(x => x.Value)
                .IsRequired();

            builder.Property(x => x.Difference)
                .IsRequired();


            builder.HasOne(x => x.MeterData)
               .WithMany(y => y.MeterValues)
               .HasForeignKey(x => x.MeterDataId);
        }
    }
}
