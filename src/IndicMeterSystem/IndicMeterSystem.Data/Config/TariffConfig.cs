﻿using IndicMeterSystem.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IndicMeterSystem.Data.Config
{
    class TariffConfig : IEntityTypeConfiguration<Tariff>
    {
        public void Configure(EntityTypeBuilder<Tariff> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.TariffTypeId)
                .IsRequired();

            builder.Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.Unit)
                .IsRequired()
                .HasMaxLength(10);

            builder.Property(x => x.Deleted)
                .HasDefaultValue(false)
                .IsRequired();

            builder.HasOne(x => x.TariffType)
                .WithMany()
                .HasForeignKey(x => x.TariffTypeId);
        }
    }
}
