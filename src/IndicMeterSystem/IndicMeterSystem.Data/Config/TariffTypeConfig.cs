﻿using IndicMeterSystem.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IndicMeterSystem.Data.Config
{
    class TariffTypeConfig : IEntityTypeConfiguration<TariffType>
    {
        public void Configure(EntityTypeBuilder<TariffType> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.SubTariffCount)
                .IsRequired();

            // Заполняем таблицу типов тарифов фиксированными значениями.
            builder.HasData(
                new TariffType[]
                {
                    new TariffType { Id = 1, Title = "Фиксированный платеж", SubTariffCount = 1 },
                    new TariffType { Id = 2, Title = "По показаниям, однотарифный", SubTariffCount = 1 },
                    new TariffType { Id = 3, Title = "По показаниям, двухтарифный", SubTariffCount = 2 }
                });

        }
    }
}
