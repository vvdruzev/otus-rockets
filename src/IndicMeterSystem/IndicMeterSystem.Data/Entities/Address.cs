﻿using System.Collections.Generic;
using System.Linq;

namespace IndicMeterSystem.Data.Entities
{
    public class Address
    {
        /// <summary>
        /// Идентификатор адреса.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Адрес пользователя.
        /// </summary>
        public string RegistrationAddress { get; set; }

        /// <summary>
        /// Уточняющая информащая к адресу пользователя.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Признак актуальности адреса.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Свойство навигации EF.
        /// </summary>		
        public User User { get; set; }

        /// <summary>
        /// Список связанных счетчиков.
        /// </summary>
        public IEnumerable<Meter> Meters { get; set; }

        /// <summary>
        /// Копирует текущую сущность c вложениями.
        /// </summary>
        /// <returns>Возвращает модель-копию.</returns>
        public Address DeepCopy()
        {
            var addressClone = new Address
            {
                UserId = UserId,
                Name = $"{Name} копия",
                RegistrationAddress = $"{RegistrationAddress}"
            };
            addressClone.Meters = Meters
                .Select(x =>
                    new Meter()
                    {
                        SerialNumber = x.SerialNumber,
                        Description = $"{x.Description}",
                        DateFrom = x.DateFrom,
                        DateTo = x.DateTo,
                        TariffId = x.TariffId
                    })
                .ToList();

            return addressClone;
        }
    }
}
