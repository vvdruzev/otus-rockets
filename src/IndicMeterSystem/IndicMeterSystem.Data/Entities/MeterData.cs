﻿using System;
using System.Collections.Generic;

namespace IndicMeterSystem.Data.Entities
{
    /// <summary>
    /// Класс для хранения общей информации по показаниям счетчика.
    /// </summary>
    public class MeterData
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата добавления.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Комментарий.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Идентификатор счетчика.
        /// </summary>
        public int MeterId { get; set; }

        /// <summary>
        /// Статус оплаты показания.
        /// </summary>
        public int PaymentState { get; set; }

        /// <summary>
        /// Список связанных платежей.
        /// </summary>
        public IEnumerable<Payment> Payments { get; set; }

        /// <summary>
        /// Список значений показаний.
        /// </summary>
        public IEnumerable<MeterValue> MeterValues { get; set; }

        /// <summary>
        /// Ссылка на связанный счетчик.
        /// </summary>
        public Meter Meter { get; set; }
    }
}
