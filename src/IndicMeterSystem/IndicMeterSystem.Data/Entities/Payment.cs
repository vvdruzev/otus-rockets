﻿using System;

namespace IndicMeterSystem.Data.Entities
{
    /// <summary>
    /// Класс для хранения информации по оплате показания.
    /// </summary>
    public class Payment
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата оплаты.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Сумма оплаты.
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// Идентификатор показания.
        /// </summary>
        public int MeterDataId { get; set; }

        /// <summary>
        /// Связанный показатель
        /// </summary>
        public MeterData MeterData { get; set; }
    }
}
