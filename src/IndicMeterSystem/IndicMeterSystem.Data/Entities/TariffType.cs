﻿using System;

namespace IndicMeterSystem.Data.Entities
{
    /// <summary>
    /// Фиксированный набор типов тарифов.  3 варианта:
    /// 1. фиксированный платеж
    /// 2. по показаниям, однотарифный
    /// 3. по показаниям, двухтарифный
    /// </summary>
    public class TariffType
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Название типа
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Количество подтарифов для тарифов данного типа
        /// </summary>
        public int SubTariffCount { get; set; }
    }
}
