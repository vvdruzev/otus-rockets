﻿using System.Collections.Generic;

namespace IndicMeterSystem.Data.Entities
{
    public class User
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя пользователя.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Логин.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Признак актуальности пользователя.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Свойство навигации EF.
        /// </summary>
        public ICollection<Address> Addresses { get; set; }

        /// <summary>
        /// Электронная почта.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Статус получения оповещений по email.
        /// </summary>
        public bool GetEmailNotification { get; set; }

        /// <summary>
        /// Статус получения оповещений по смс.
        /// </summary>
        public bool GetSmsNotification { get; set; }
    }
}