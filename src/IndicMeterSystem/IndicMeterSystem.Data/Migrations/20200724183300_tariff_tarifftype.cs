﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class tariff_tarifftype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Tariffs_TariffTypeId",
                table: "Tariffs",
                column: "TariffTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tariffs_TariffTypes_TariffTypeId",
                table: "Tariffs",
                column: "TariffTypeId",
                principalTable: "TariffTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tariffs_TariffTypes_TariffTypeId",
                table: "Tariffs");

            migrationBuilder.DropIndex(
                name: "IX_Tariffs_TariffTypeId",
                table: "Tariffs");
        }
    }
}
