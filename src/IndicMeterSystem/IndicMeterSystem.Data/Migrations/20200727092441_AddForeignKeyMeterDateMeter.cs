﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class AddForeignKeyMeterDateMeter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_MeterDatas_MeterId",
                table: "MeterDatas",
                column: "MeterId");

            migrationBuilder.AddForeignKey(
                name: "FK_MeterDatas_Meters_MeterId",
                table: "MeterDatas",
                column: "MeterId",
                principalTable: "Meters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MeterDatas_Meters_MeterId",
                table: "MeterDatas");

            migrationBuilder.DropIndex(
                name: "IX_MeterDatas_MeterId",
                table: "MeterDatas");
        }
    }
}
