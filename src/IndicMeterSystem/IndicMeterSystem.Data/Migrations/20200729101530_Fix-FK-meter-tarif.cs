﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class FixFKmetertarif : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tariffs_Meters_MeterId",
                table: "Tariffs");

            migrationBuilder.DropIndex(
                name: "IX_Tariffs_MeterId",
                table: "Tariffs");

            migrationBuilder.DropColumn(
                name: "MeterId",
                table: "Tariffs");

            migrationBuilder.CreateIndex(
                name: "IX_Meters_TarrifId",
                table: "Meters",
                column: "TarrifId");

            migrationBuilder.AddForeignKey(
                name: "FK_Meters_Tariffs_TarrifId",
                table: "Meters",
                column: "TarrifId",
                principalTable: "Tariffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meters_Tariffs_TarrifId",
                table: "Meters");

            migrationBuilder.DropIndex(
                name: "IX_Meters_TarrifId",
                table: "Meters");

            migrationBuilder.AddColumn<int>(
                name: "MeterId",
                table: "Tariffs",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Tariffs_MeterId",
                table: "Tariffs",
                column: "MeterId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tariffs_Meters_MeterId",
                table: "Tariffs",
                column: "MeterId",
                principalTable: "Meters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
