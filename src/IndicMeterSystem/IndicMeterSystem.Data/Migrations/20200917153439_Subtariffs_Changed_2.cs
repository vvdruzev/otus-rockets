﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class Subtariffs_Changed_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_SubTariffs_TariffId_Number",
                table: "SubTariffs");

            migrationBuilder.DropColumn(
                name: "Actual",
                table: "SubTariffs");

            migrationBuilder.DropColumn(
                name: "Value",
                table: "SubTariffs");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AddDate",
                table: "SubTariffs",
                nullable: false,
                defaultValue: new DateTime(2020, 9, 17, 18, 34, 38, 799, DateTimeKind.Local).AddTicks(8033),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AddColumn<int>(
                name: "Price",
                table: "SubTariffs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_SubTariffs_TariffId",
                table: "SubTariffs",
                column: "TariffId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SubTariffs_TariffId",
                table: "SubTariffs");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "SubTariffs");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AddDate",
                table: "SubTariffs",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 9, 17, 18, 34, 38, 799, DateTimeKind.Local).AddTicks(8033));

            migrationBuilder.AddColumn<bool>(
                name: "Actual",
                table: "SubTariffs",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Value",
                table: "SubTariffs",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_SubTariffs_TariffId_Number",
                table: "SubTariffs",
                columns: new[] { "TariffId", "Number" });
        }
    }
}
