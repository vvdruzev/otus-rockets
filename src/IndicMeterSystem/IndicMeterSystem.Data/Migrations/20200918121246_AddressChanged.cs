﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class AddressChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "AddDate",
                table: "SubTariffs",
                nullable: false,
                defaultValue: new DateTime(2020, 9, 18, 17, 12, 46, 488, DateTimeKind.Local).AddTicks(36),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2020, 9, 17, 18, 34, 38, 799, DateTimeKind.Local).AddTicks(8033));

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "Adresses",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Adresses",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "Adresses");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Adresses");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AddDate",
                table: "SubTariffs",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2020, 9, 17, 18, 34, 38, 799, DateTimeKind.Local).AddTicks(8033),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 9, 18, 17, 12, 46, 488, DateTimeKind.Local).AddTicks(36));
        }
    }
}
