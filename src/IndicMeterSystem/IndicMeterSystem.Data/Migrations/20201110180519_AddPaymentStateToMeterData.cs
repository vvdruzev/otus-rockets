﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class AddPaymentStateToMeterData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PaymentState",
                table: "MeterDatas",
                nullable: false,
                defaultValue: 1);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentState",
                table: "MeterDatas");
        }
    }
}
