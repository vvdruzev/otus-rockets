﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class TarifId_To_TariffId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meters_Tariffs_TarifId",
                table: "Meters");

            migrationBuilder.DropIndex(
                name: "IX_Meters_TarifId",
                table: "Meters");

            migrationBuilder.DropColumn(
                name: "TarifId",
                table: "Meters");

            migrationBuilder.AddColumn<int>(
                name: "TariffId",
                table: "Meters",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Meters_TariffId",
                table: "Meters",
                column: "TariffId");

            migrationBuilder.AddForeignKey(
                name: "FK_Meters_Tariffs_TariffId",
                table: "Meters",
                column: "TariffId",
                principalTable: "Tariffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meters_Tariffs_TariffId",
                table: "Meters");

            migrationBuilder.DropIndex(
                name: "IX_Meters_TariffId",
                table: "Meters");

            migrationBuilder.DropColumn(
                name: "TariffId",
                table: "Meters");

            migrationBuilder.AddColumn<int>(
                name: "TarifId",
                table: "Meters",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Meters_TarifId",
                table: "Meters",
                column: "TarifId");

            migrationBuilder.AddForeignKey(
                name: "FK_Meters_Tariffs_TarifId",
                table: "Meters",
                column: "TarifId",
                principalTable: "Tariffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
