﻿using IndicMeterSystem.Data.EF;
using IndicMeterSystem.Data.Interfaces;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IndicMeterSystem.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T :class
    {
        private ApplicationContext _db;
        private DbSet<T> _dbSet;

        public Repository(ApplicationContext context)
        {
            _db = context;
            _dbSet = context.Set<T>();
        }

        public async Task<T> AddAsync(T item)
        {
            await _dbSet.AddAsync(item);
            await _db.SaveChangesAsync();

            return item;
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbSet
                .AsNoTracking()
                .AnyAsync(predicate);
        }

        public async Task DeleteAsync(int id)
        {
            T item = await _dbSet.FindAsync(id);
            if (item != null)
                _dbSet.Remove(item);
            await _db.SaveChangesAsync();
        }

        public async Task<T> GetAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbSet.AsNoTracking().ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWithIncludeOnConditionAsync(string predicateIncl, 
            Expression<Func<T, bool>> predicateCond)
        {
            return await _dbSet
                .AsNoTracking()
                .Where(predicateCond)
                .Include(predicateIncl)
                .ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWithIncludeOnConditionAsync(string predicateIncl1,
            string predicateIncl2, Expression<Func<T, bool>> predicateCond)
        {
            return await _dbSet
                .AsNoTracking()
                .Where(predicateCond)
                .Include(predicateIncl1)
                .Include(predicateIncl2)
                .ToListAsync();
        }

        public async Task<IEnumerable<T>> GetOnConditionAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbSet.AsNoTracking().Where(predicate).ToListAsync();
        }

        public async Task UpdateAsync(T item)
        {
            _db.Entry(item).State = EntityState.Modified;
            await _db.SaveChangesAsync();
        }
    }
}
