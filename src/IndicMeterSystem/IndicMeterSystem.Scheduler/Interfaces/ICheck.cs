﻿using System.Collections.Generic;

namespace IndicMeterSystem.Scheduler.Interfaces
{
    /// <summary>
    /// Интерфейс отправки данных в EventBus.
    /// </summary>
    public interface ICheck
    {
        /// <summary>
        /// Отправляет в EventBus сообщения для оповещения.
        /// </summary>
        void Execute();
    }
}
