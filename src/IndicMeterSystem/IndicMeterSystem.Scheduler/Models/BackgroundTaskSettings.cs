﻿namespace IndicMeterSystem.Scheduler
{
    /// <summary>
    /// Класс настроек.
    /// </summary>
    public class BackgroundTaskSettings
    {
        /// <summary>
        /// Строка подключения к базе.
        /// </summary>
        public string DatabaseConnectionStrings { get; set; }

        /// <summary>
        /// Интервал работы задачи в миллисекундах.
        /// </summary>
        public int TaskRunInterval { get; set; }

        /// <summary>
        /// День месяца начала периода проверки неоплаченных показаний.
        /// </summary>
        public int MonthDayCheckPaymentStart { get; set; }

        /// <summary>
        /// День месяца окончания периода проверки неоплаченных показаний.
        /// </summary>
        public int MonthDayCheckPaymentEnd { get; set; }

        /// <summary>
        /// День месяца начала периода проверки отсутствия показаний.
        /// </summary>
        public int MonthDayCheckMeterDataStart { get; set; }

        /// <summary>
        /// День месяца окончания периода проверки отсутствия показаний.
        /// </summary>
        public int MonthDayCheckMeterDataEnd { get; set; }

        /// <summary>
        /// Жизненный цикл события в днях.
        /// </summary>
        public int EventLifeTimeDays { get; set; }
    }
}
