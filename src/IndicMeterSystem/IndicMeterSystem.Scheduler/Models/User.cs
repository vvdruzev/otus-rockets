﻿using System;

namespace IndicMeterSystem.Scheduler
{
    /// <summary>
    /// Данные пользователя.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Электронная почта пользователя.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона пользователя.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Адресс пользователя.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Дата внесения показания в базу.
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Статус получения оповещений по email.
        /// </summary>
        public bool GetEmailNotification { get; set; }

        /// <summary>
        /// Статус получения оповещений по смс.
        /// </summary>
        public bool GetSmsNotification { get; set; }
    }
}
