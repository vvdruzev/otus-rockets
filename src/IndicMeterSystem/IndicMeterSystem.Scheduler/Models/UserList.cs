﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Npgsql;

using System;
using System.Collections.Generic;
using System.Linq;

namespace IndicMeterSystem.Scheduler
{
    /// <summary>
    /// Работает со списком моделей User.
    /// </summary>
    public class UserList
    {
        private IEnumerable<User> _users;
        private readonly ILogger<UserList> _logger;
        private readonly BackgroundTaskSettings _settings;

        public UserList(
            IOptions<BackgroundTaskSettings> settings,
            ILogger<UserList> logger)
        {
            _settings = settings?.Value ?? throw new ArgumentNullException(nameof(settings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <summary>
        /// Получает данные пользов. с неоплаченными показаниями.
        /// </summary>
        /// <returns>Список данных пользователей.</returns>
        public IEnumerable<User> GetUnpaidMeterData()
        {
             _users = Enumerable.Empty<User>();

            using var conn = new NpgsqlConnection(_settings.DatabaseConnectionStrings);

            try
            {
                conn.Open();

                using var cmd = new NpgsqlCommand(Queries.GetDataWithoutPayment, conn);
                cmd.Prepare();

                using NpgsqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    _users = _users.Append(new User()
                    {
                        Address = rdr.GetString(0),
                        Email = rdr.GetString(1),
                        Date = rdr.GetDateTime(2),
                        PhoneNumber = rdr.IsDBNull(3) ? default : rdr.GetString(3),
                        GetEmailNotification = rdr.GetBoolean(4),
                        GetSmsNotification = rdr.GetBoolean(5)
                    });
                }
            }
            catch (NpgsqlException exception)
            {
                _logger.LogCritical(
                    exception,
                    "FATAL ERROR: Database connections could not be opened: {Message}",
                    exception.Message);
            }

            return _users;
        }

        /// <summary>
        /// Получает данные пользов. с отсутствующими показаниями.
        /// </summary>
        /// <returns>Список данных пользователей.</returns>
        public IEnumerable<User> GetMissingMeterData()
        {
            _users = Enumerable.Empty<User>();

            using var conn = new NpgsqlConnection(_settings.DatabaseConnectionStrings);

            try
            {
                conn.Open();

                var firstDayOfMonth = 1;
                var dateStart = new DateTime(
                    DateTime.Today.Year, 
                    DateTime.Today.Month, 
                    firstDayOfMonth);
                var dateEnd = dateStart.AddMonths(1);

                using var cmd = new NpgsqlCommand(Queries.GetDataWithoutMeterData, conn);
                cmd.Parameters.AddWithValue("DateStart", dateStart);
                cmd.Parameters.AddWithValue("DateEnd", dateEnd);
                cmd.Prepare();

                using NpgsqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    _users = _users.Append(new User()
                    {
                        Address = rdr.GetString(0),
                        Email = rdr.GetString(1),
                        PhoneNumber = rdr.IsDBNull(2) ? default : rdr.GetString(2),
                        GetEmailNotification = rdr.GetBoolean(3),
                        GetSmsNotification = rdr.GetBoolean(4)
                    });
                }
            }
            catch (NpgsqlException exception)
            {
                _logger.LogCritical(
                    exception,
                    "FATAL ERROR: Database connections could not be opened: {Message}",
                    exception.Message);
            }

            return _users;
        }
    }
}
