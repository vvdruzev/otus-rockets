﻿using BusinesEvent;

using IndicMeterSystem.Scheduler.Interfaces;
using Infrastructure.EventBus.EventBusInfrastructure.Abstraction;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System;

namespace IndicMeterSystem.Scheduler.Notifications
{
    public class MeterDataSmsNotif : INotification
    {
        private readonly ILogger<MeterDataSmsNotif> _logger;
        private readonly BackgroundTaskSettings _settings;
        private readonly IEventBus _eventBus;

        public MeterDataSmsNotif(
            IOptions<BackgroundTaskSettings> settings,
            ILogger<MeterDataSmsNotif> logger,
            IEventBus eventBus)
        {
            _settings = settings?.Value ?? throw new ArgumentNullException(nameof(settings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public void SendNotification(User data)
        {
            if (data.PhoneNumber == null)
            {
                return;
            }

            using var notificationEvent = new UserMeterDataSmsNotifEvent(
                data.PhoneNumber,
                DateTime.Today.AddDays(_settings.EventLifeTimeDays),
                data.Address);

            _logger.LogInformation("----- Publishing integration event: " +
                "{IntegrationEventId} from {AppName} - ({@IntegrationEvent})",
                notificationEvent.Id, Program.AppName, notificationEvent);

            _eventBus.Publish(notificationEvent);
        }
    }
}
