﻿using IndicMeterSystem.Scheduler.Notifications;
using IndicMeterSystem.Scheduler.Tasks;
using IndicMeterSystem.Scheduler.Сhecks;

using Infrastructure.EventBus.EventBusRabbitMQ;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IndicMeterSystem.Scheduler
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.Configure<BackgroundTaskSettings>(this.Configuration)
                    .AddOptions()
                    .AddEventBus(this.Configuration)
                    .AddScoped<UserList>()
                    .AddSingleton<NotificationContext>()
                    .AddSingleton<PaymentEmailNotif>()
                    .AddSingleton<PaymentSmsNotif>()
                    .AddSingleton<MeterDataEmailNotif>()
                    .AddSingleton<MeterDataSmsNotif>()
                    .AddScoped<CheckPaymentExist>()
                    .AddScoped<CheckMeterDataExist>()
                    .AddHostedService<CheckPaymentManagerTask>()
                    .AddHostedService<CheckMeterDataManagerTask>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Start Scheduler");
                });
            });
        }
    }
}
