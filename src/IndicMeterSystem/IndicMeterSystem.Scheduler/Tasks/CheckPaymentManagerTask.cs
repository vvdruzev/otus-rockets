﻿using IndicMeterSystem.Scheduler.Сhecks;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace IndicMeterSystem.Scheduler.Tasks
{
    public class CheckPaymentManagerTask : BackgroundService
    {
        private readonly ILogger<CheckPaymentManagerTask> _logger;
        private readonly BackgroundTaskSettings _settings;
        private readonly CheckPaymentExist _checkPaymentExist;

        public CheckPaymentManagerTask(
            IOptions<BackgroundTaskSettings> settings,
            ILogger<CheckPaymentManagerTask> logger,
            CheckPaymentExist checkPaymentExist)
        {
            _settings = settings?.Value ?? throw new ArgumentNullException(nameof(settings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _checkPaymentExist = checkPaymentExist ?? throw new ArgumentNullException(nameof(checkPaymentExist));
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogDebug("CheckPaymentManagerTask is starting.");

            stoppingToken.Register(() => _logger.LogDebug(
                "#1 CheckPaymentManagerTask background task is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogDebug(
                    "CheckPaymentManagerTask background task is doing background work.");

                // Проверяем дату на вхождение в период проверки.
                if (DateTime.Now.Day >= _settings.MonthDayCheckPaymentStart
                    && DateTime.Now.Day <= _settings.MonthDayCheckPaymentEnd) 
                {
                    _checkPaymentExist.Execute();
                }

                await Task.Delay(_settings.TaskRunInterval, stoppingToken);
            }

            _logger.LogDebug("CheckPaymentManagerTask background task is stopping.");

            await Task.CompletedTask;
        }
    }
}
