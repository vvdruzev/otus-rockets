﻿using IndicMeterSystem.Scheduler.Interfaces;
using IndicMeterSystem.Scheduler.Notifications;

using Microsoft.Extensions.Logging;

using System;

namespace IndicMeterSystem.Scheduler.Сhecks
{
    public class CheckMeterDataExist : ICheck
    {
        private readonly ILogger<CheckMeterDataExist> _logger;
        private readonly UserList _userList;
        private readonly NotificationContext _notificationContext;
        private readonly MeterDataEmailNotif _emailNotification;
        private readonly MeterDataSmsNotif _smsNotification;

        public CheckMeterDataExist(
            ILogger<CheckMeterDataExist> logger,
            UserList userList,
            NotificationContext notificationContext,
            MeterDataEmailNotif emailNotification,
            MeterDataSmsNotif smsNotification)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userList = userList ?? throw new ArgumentNullException(nameof(userList));
            _notificationContext = notificationContext ?? throw new ArgumentNullException(nameof(notificationContext));
            _emailNotification = emailNotification ?? throw new ArgumentNullException(nameof(emailNotification));
            _smsNotification = smsNotification ?? throw new ArgumentNullException(nameof(smsNotification));
        }

        public void Execute()
        {
            _logger.LogDebug("Check for missing meter data.");

            var users = _userList.GetMissingMeterData();

            foreach (var user in users)
            {
                if (user.GetSmsNotification && user.GetEmailNotification)
                {
                    _notificationContext.SetNotification(_smsNotification);
                    _notificationContext.Execute(user);
                    _notificationContext.SetNotification(_emailNotification);
                }
                else if (user.GetEmailNotification)
                {
                    _notificationContext.SetNotification(_emailNotification);
                }
                else if (user.GetSmsNotification)
                {
                    _notificationContext.SetNotification(_smsNotification);
                }

                _notificationContext.Execute(user);
            }
        }
    }
}
