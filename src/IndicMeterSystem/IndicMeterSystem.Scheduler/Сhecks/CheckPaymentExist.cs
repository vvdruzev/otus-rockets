﻿using IndicMeterSystem.Scheduler.Interfaces;
using IndicMeterSystem.Scheduler.Notifications;

using Microsoft.Extensions.Logging;

using System;

namespace IndicMeterSystem.Scheduler.Сhecks
{
    public class CheckPaymentExist : ICheck
    {
        private readonly ILogger<CheckPaymentExist> _logger;
        private readonly UserList _userList;
        private readonly NotificationContext _notificationContext;
        private readonly PaymentEmailNotif _emailNotification;
        private readonly PaymentSmsNotif _smsNotification;

        public CheckPaymentExist(
            ILogger<CheckPaymentExist> logger,
            UserList userList,
            NotificationContext notificationContext,
            PaymentEmailNotif emailNotification,
            PaymentSmsNotif smsNotification)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userList = userList ?? throw new ArgumentNullException(nameof(userList));
            _notificationContext = notificationContext ?? throw new ArgumentNullException(nameof(notificationContext));
            _emailNotification = emailNotification ?? throw new ArgumentNullException(nameof(emailNotification));
            _smsNotification = smsNotification ?? throw new ArgumentNullException(nameof(smsNotification));
        }

        public void Execute()
        {
            _logger.LogDebug("Check for unpaid meter data.");

            var users = _userList.GetUnpaidMeterData();

            foreach (var user in users)
            {
                if (user.GetSmsNotification && user.GetEmailNotification)
                {
                    _notificationContext.SetNotification(_smsNotification);
                    _notificationContext.Execute(user);
                    _notificationContext.SetNotification(_emailNotification);
                }
                else if (user.GetEmailNotification)
                {
                    _notificationContext.SetNotification(_emailNotification);
                }
                else if (user.GetSmsNotification)
                {
                    _notificationContext.SetNotification(_smsNotification);
                }
                
                _notificationContext.Execute(user);
            }
        }
    }
}
