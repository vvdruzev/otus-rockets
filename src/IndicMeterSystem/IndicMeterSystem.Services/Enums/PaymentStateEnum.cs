﻿namespace IndicMeterSystem.Services.Enums
{
    public enum PaymentStateEnum
    {
        None = 1,
        Partial = 2,
        Full = 3
    }
}
