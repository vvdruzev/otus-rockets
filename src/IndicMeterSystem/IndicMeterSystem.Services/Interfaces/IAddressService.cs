﻿using IndicMeterSystem.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Interfaces
{
    public interface IAddressService
    {
        /// <summary>
        /// Метод получает адреса по id пользователя.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <returns>Возвращает список адресов пользователя.</returns>
        Task<IEnumerable<AddressModel>> GetByUserIdAsync(int userId);

        /// <summary>
        /// Добавление адреса пользователя.
        /// </summary>
        /// <param name="address">Данные адреса.</param>
        Task AddAddressAsync(AddressModel address);

        /// <summary>
        /// Присвоение адресу архивного статуса.
        /// </summary>
        /// <param name="addressId">Идентификатор адреса.</param>
        Task DeleteAddressAsync(int addressId);

        /// <summary>
        /// Копирует адрес и сохраняет в базу.
        /// </summary>
        /// <param name="addressId">Идентификатор адреса.</param>
        /// <returns>Возвращает модель-копию адреса.</returns>
        Task<InclusiveAddressModel> AddCopyAsync(int addressId);

        /// <summary>
        /// Изменяет адрес.
        /// </summary>
        /// <param name="address">Изменяемая модель адреса.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        Task UpdateAddressAsync(AddressModel address);
    }
}
