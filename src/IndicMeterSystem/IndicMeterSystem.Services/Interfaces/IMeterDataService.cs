﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Interfaces
{
    /// <summary>
    /// Интерфейс для работы с показаниями.
    /// </summary>
    public interface IMeterDataService
    {
        /// <summary>
        /// Добавляет новую запись показания.
        /// </summary>
        /// <param name="meterDataModel">Модель с данными по показанию.</param>
        Task<MeterDataModel> AddAsync(MeterDataModel meterDataModel);

        /// <summary>
        /// Проверяет наличие записи показания за период.
        /// </summary>
        /// <param name="date">Период на который идет поиск.</param>
        /// <param name="meterId">Идентификатор счетчика.</param>
        /// <returns>Возвращает true если значение найдено, иначе false.</returns>
        Task<bool> ExistAsync(DateTime date, int meterId);

        /// <summary>
        /// Получает данные показаний счетчика на определенную дату.
        /// </summary>
        /// <param name="date">Дата для поиска показания.</param>
        /// <param name="meterId">Идентификатор счетчика.</param>
        /// <returns>Возвращает модель данных.</returns>
        Task<MeterDataWithSumModel> GetByDateAsync(DateTime date, int meterId);

        /// <summary>
        /// Получает данные по показанию.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает модель показания.</returns>
        Task<MeterDataModel> GetByIdAsync(int meterDataId);

        /// <summary>
        /// Получает данные по показанию и его платежи.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает модель показания со списком платежей.</returns>
        Task<MeterDataWithPaymentsModel> GetByIdWithPaymentsAsync(int meterDataId);

        /// <summary>
        /// Получает данные показаний счетчика за период.
        /// </summary>
        /// <param name="dateFrom">Начальная дата интервала.</param>
        /// <param name="dateTo">Конечная дата интервала.</param>
        /// <param name="meterId">Идентификатор счетчика, может быть не задан.</param>
        /// <returns>Возвращает список моделей данных.</returns>
        Task<IEnumerable<MeterDataWithSumModel>> GetByIntervalAsync(DateTime dateFrom, DateTime dateTo, int meterId = 0);
    }
}
