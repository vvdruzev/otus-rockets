using IndicMeterSystem.Services.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Interfaces
{
    public interface IMeterService
    {
        /// <summary>
        /// Метод получает счетчики по id адреса.
        /// </summary>
        /// <param name="addressId">Идентификатор адреса.</param>
        /// <returns>Возвращает список счетчиков</returns>
        Task<IEnumerable<MeterModel>> GetByAddressIdAsync(int addressId);

        /// <summary>
        /// Добавление счетчика.
        /// </summary>
        /// <param name="meter">Данные счетчика.</param>
        Task AddMeterAsync(MeterModel meter);

        /// <summary>
        /// Получает данные счетчика по его идентификатору.
        /// </summary>
        /// <param name="meterId">Идентификатор счетчика.</param>
        /// <returns>Вовращает данные счетчика.</returns>
        Task<MeterModel> GetByIdAsync(int meterId);

        /// <summary>
        /// Копирует счетчик.
        /// </summary>
        /// <param name="meterId">Идентификатор счетчика.</param>
        /// <returns>Возвращает модель-копию счетчика.</returns>
        Task<MeterModel> GetCopyAsync(int meterId);

        /// <summary>
        /// Изменение счетчика.
        /// </summary>
        /// <param name="meter">Данные счетчика.</param>
        Task UpdateMeterAsync(MeterModel meter);

        /// <summary>
        /// Присвоение счетчику архивного статуса.
        /// </summary>
        /// <param name="meterId">Идентификатор счетчика.</param>
        Task DeleteMeterAsync(int meterId);
    }
}
