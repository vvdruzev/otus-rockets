﻿using IndicMeterSystem.Services.Models;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Получение пользователя по логину и паролю.
        /// </summary>
        /// <param name="login">Логин.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>Данные пользователя.</returns>
        Task<UserModel> GetUserByLoginAsync(string login, string password);

        /// <summary>
        /// Добавление пользователя.
        /// </summary>
        /// <param name="user">Данные пользователя.</param>
        Task AddUserAsync(UserModel user);

        /// <summary>
        /// Удаление пользователя в архив.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя.</param>
        Task DeleteUserAsync(int userId);

        /// <summary>
        /// Получение данных пользователя по Id.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <returns>Модель пользователя.</returns>
        Task<UserModel> GetByIdAsync(int userId);

        /// <summary>
        /// Изменяет данные пользователя.
        /// </summary>
        /// <param name="user">Изменяемая модель пользователя.</param>
        /// <returns></returns>
        Task UpdateUserAsync(UserModel user);
    }
}
