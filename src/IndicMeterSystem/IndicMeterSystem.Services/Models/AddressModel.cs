﻿using System.Text.Json.Serialization;

namespace IndicMeterSystem.Services.Models
{
    public class AddressModel
    {
        /// <summary>
        /// Идентификатор адреса.
        /// </summary>
        /// 
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Адрес пользователя.
        /// </summary>
        public string RegistrationAddress { get; set; }

        /// <summary>
        /// Уточняющая информащая к адресу пользователя.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Признак актуальности адреса.
        /// </summary>
        public bool Deleted { get; set; }

    }
}
