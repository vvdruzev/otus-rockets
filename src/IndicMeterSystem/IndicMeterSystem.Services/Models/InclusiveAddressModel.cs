﻿using System.Collections.Generic;

namespace IndicMeterSystem.Services.Models
{
    public class InclusiveAddressModel
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Адрес пользователя.
        /// </summary>
        public string RegistrationAddress { get; set; }

        /// <summary>
        /// Уточняющая информащая к адресу пользователя.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Список связанных счетчиков.
        /// </summary>
        public IEnumerable<MeterModel> Meters { get; set; }
    }
}
