﻿using System;
using System.Text.Json.Serialization;

namespace IndicMeterSystem.Services.Models
{
    public class MeterModel
    {
        /// <summary>
        /// Id счетчика
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Серийный номер
        /// </summary>
        public int SerialNumber { get; set; }

        /// <summary>
        /// Id адреса
        /// </summary>
        public int AddressId { get; set; }

        /// <summary>
        /// Описание счетчика
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Дата установки
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Дата замены
        /// </summary>
        public DateTime? DateTo { get; set; }

        /// <summary>
        /// Id тарифного плана
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Признак актуальности счетчика.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Копирует текущую модель без вложений.
        /// </summary>
        /// <returns></returns>
        public MeterModel ShallowCopy()
        {
            var meter =  (MeterModel)MemberwiseClone();
            meter.Id = 0;
            meter.SerialNumber = 0;
            meter.Description = string.Empty;

            return meter;
        }
    }
}
