﻿using System;
using System.Text.Json.Serialization;

namespace IndicMeterSystem.Services.Models
{
    /// <summary>
    /// Модель с данными подтарифа.
    /// </summary>
    public class SubTariffModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// Id тарифа
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Порядковый номер подтариыа в тарифе. Начинается с "1" для каждого тарифа
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Цена за единицу
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Дата добавления тарифа
        /// </summary>
        public DateTime AddDate { get; set; }
    }
}
