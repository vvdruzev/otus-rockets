using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;

using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Common.Properties;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Services
{
    /// <summary>
    /// Реализует интерфейс показаний. 
    /// </summary>
    public class MeterDataService : IMeterDataService
    {
        private readonly IRepository<MeterData> _meterDataRepository;
        private readonly IPaymentService _paymentService;
        private readonly IMapper _mapper;

        public MeterDataService(
            IRepository<MeterData> meterDataRepository,
            IPaymentService paymentService,
            IMapper mapper)
        {
            _meterDataRepository = meterDataRepository;
            _paymentService = paymentService;
            _mapper = mapper;
        }

        public async Task<MeterDataModel> AddAsync(MeterDataModel addMeterDataModel)
        {
            ValidateModel(addMeterDataModel);

            // Проверка на существование показания в текущем месяце.
            var isExist = await ExistAsync(
                DateTime.Now,
                addMeterDataModel.MeterId);

            if (isExist)
            {
                throw new ServiceException(nameof(ErrorMessages.IMS30001));
            }

            // Находим прошлое значение показания.
            var oldMeterData = (await GetByDateAsync(
                addMeterDataModel.Date.AddMonths(-1),
                addMeterDataModel.MeterId));

            foreach (var meterValue in addMeterDataModel.MeterValues)
            {
                var meterValueNew = oldMeterData?.MeterValues
                    .Where(x => x.Number == meterValue.Number)
                    .Single();

                var difference = meterValue.Value - (meterValueNew?.Value ?? 0);

                if (difference < 0)
                {
                    throw new ServiceException(nameof(ErrorMessages.IMS30002));
                }

                meterValue.Difference = difference;
            }            

            var meterData = _mapper.Map<MeterDataModel, MeterData>(
                addMeterDataModel,
                opts => opts.AfterMap((s, d) =>
                {
                    d.Date = DateTime.Now;
                }));

            addMeterDataModel = _mapper.Map<MeterDataModel>(
                await _meterDataRepository.AddAsync(meterData));

            return addMeterDataModel;
        }

        public async Task<bool> ExistAsync(DateTime date, int meterId)
        {
            var dateStart = new DateTime(date.Year, date.Month, 1);
            var dateEnd = dateStart.AddMonths(1);

            var isExist = await _meterDataRepository
                .AnyAsync(x => x.Date < dateEnd
                    && x.Date >= dateStart
                    && x.MeterId == meterId);

            return isExist;
        }

        public async Task<MeterDataModel> GetByIdAsync(int meterDataId)
        {
            var meterData = (await _meterDataRepository
                .GetWithIncludeOnConditionAsync(
                    nameof(MeterDataModel.MeterValues),
                    x => x.Id == meterDataId))
                .FirstOrDefault();

            meterData.EnsureExists(nameof(ErrorMessages.IMS40001));

            var meterDataModel = _mapper.Map<MeterDataModel>(meterData);

            return meterDataModel;
        }

        public async Task<MeterDataWithPaymentsModel> GetByIdWithPaymentsAsync(int meterDataId)
        {
            var meterData = (await _meterDataRepository
                .GetWithIncludeOnConditionAsync(
                    nameof(MeterDataWithPaymentsModel.Payments),
                    nameof(MeterDataWithPaymentsModel.MeterValues),
                    x => x.Id == meterDataId))
                .FirstOrDefault();

            meterData.EnsureExists(nameof(ErrorMessages.IMS40001));

            var meterDataModel = _mapper.Map<MeterDataWithPaymentsModel>(meterData);

            // Заполнение сумм.
            await SetAmounts(meterDataModel);

            return meterDataModel;
        }

        public async Task<MeterDataWithSumModel> GetByDateAsync(
            DateTime date, int meterId)
        {
            var dateFrom = new DateTime(date.Year, date.Month, 1);
            var dateTo = dateFrom.AddMonths(1);

            // В течении месяца показания вносятся один раз.
            var meterDataModel = (await GetByIntervalAsync(dateFrom, dateTo, meterId))
                .FirstOrDefault();

            return meterDataModel;
        }

        public async Task<IEnumerable<MeterDataWithSumModel>> GetByIntervalAsync(
            DateTime dateFrom, DateTime dateTo,  int meterId = 0)
        {
            var meterDatas = await _meterDataRepository
                .GetWithIncludeOnConditionAsync(
                    nameof(MeterDataModel.MeterValues),
                    x => x.Date < dateTo
                        && x.Date >= dateFrom
                        && (meterId == 0 || x.MeterId == meterId));

            var meterDataModels =  _mapper.Map<IEnumerable<MeterDataWithSumModel>>(meterDatas);

            // Заполняем суммы.
            foreach (var item in meterDataModels)
            {
                await SetAmounts(item);
            }

            return meterDataModels;
        }

        /// <summary>
        /// Заполняет поля сумм значениями для модели MeterDataWithPaymentsModel.
        /// </summary>
        /// <param name="meterDataModel">Модель для заполнения.</param>
        /// <returns></returns>
        private async Task SetAmounts(MeterDataWithPaymentsModel meterDataModel)
        {
            meterDataModel.TotalSum = await _paymentService.GetTotalSumAsync(meterDataModel.Id);
            var paidSum = await _paymentService.GetPaidSumAsync(meterDataModel.Id);
            meterDataModel.Residue = meterDataModel.TotalSum - paidSum;
        }

        /// <summary>
        /// Заполняет поля сумм значениями для модели MeterDataWithSumModel.
        /// </summary>
        /// <param name="meterDataModel">Модель для заполнения.</param>
        /// <returns></returns>
        private async Task SetAmounts(MeterDataWithSumModel meterDataModel)
        {
            meterDataModel.TotalSum = await _paymentService.GetTotalSumAsync(meterDataModel.Id);
            var paidSum = await _paymentService.GetPaidSumAsync(meterDataModel.Id);
            meterDataModel.Residue = meterDataModel.TotalSum - paidSum;
        }

        /// <summary>
        /// Производит валидацию данных.
        /// </summary>
        /// <param name="meterDataModel">Модель показания.</param>
        private void ValidateModel(MeterDataModel meterDataModel)
        {
            if (meterDataModel == null)
            {
                throw new ValidationException(nameof(meterDataModel),
                    "Аргумент не может быть пустым.");
            }

            foreach (var meterValue in meterDataModel.MeterValues)
            {
                if (meterValue.Value == default)
                {
                    throw new ValidationException(nameof(meterValue.Value),
                        "Аргумент \"value\" не может быть равным нулю.");
                }

                if (meterValue.Number < 1)
                {
                    throw new ValidationException(nameof(meterValue.Number),
                        "Аргумент \"number\" не может быть меньше единицы.");
                }
            }
        }
    }
}
