﻿using System;
using IndicMeterSystem.Common.Exceptions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using IndicMeterSystem.Common.Properties;

namespace IndicMeterSystem.Services.Services
{
    /// <summary>
    /// Реализует интерфейс подтарифов
    /// </summary>
    public class SubTariffService : ISubTariffService
    {
        private readonly IRepository<SubTariff> _subTariffRepository;
        private readonly IRepository<Tariff> _tariffRepository;
        private readonly IMapper _mapper;

        public SubTariffService(
            IRepository<SubTariff> subTariffRepository,
            IRepository<Tariff> tariffRepository, 
            IMapper mapper)
        {
            _subTariffRepository = subTariffRepository;
            _tariffRepository = tariffRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получаем по Id тарифа список подтарифов с максимальной датой
        /// </summary>
        /// <param name="tariffId">Id тарифа для выбора подтарифа</param>
        /// <param name="maxDate">Выбираем только записи с датой, меньшей данного значения. Если null, то ищем по всем записям</param>
        /// <returns>Список подтарифов указанного тарифа с максимальной датой</returns>
        public async Task<IEnumerable<SubTariffModel>> GetByTariffIdAsync(int tariffId, DateTime? maxDate = null)
        {
            //Поскольку последние записи подтарифов могут иметь разную дату, то ищем отдельно по каждому номеру
            var subTariffCount = await GetSubTariffCountByTariffId(tariffId);
            List<SubTariffModel> subTariffList = new List<SubTariffModel>();
            for (var n = 1; n <= subTariffCount; n++)
            {
                var all = await _subTariffRepository.GetOnConditionAsync(
                    p => p.TariffId == tariffId && 
                    p.Number == n &&
                    (maxDate == null || p.AddDate <= maxDate));
                if (all == null) continue;
                var subTariffData = all.Where(p => p.AddDate == all.Max(p => p.AddDate));
                var subTariffModel = _mapper.Map<IEnumerable<SubTariffModel>>(subTariffData).FirstOrDefault();
                subTariffList.Add(subTariffModel);
            }
            return subTariffList;
        }

        /// <summary>
        /// Создание нового подтарифа
        /// </summary>
        public async Task AddAsync(SubTariffModel subTariffModel)
        {
            //Проверяем входящие данные
            if (subTariffModel == null)
                throw new ValidationException(nameof(subTariffModel), "Аргумент не может быть пустым");
            await ValidateSubTariff(subTariffModel);

            var subTariff = _mapper.Map<SubTariff>(subTariffModel);
            await _subTariffRepository.AddAsync(subTariff);

        }

        //Проверяем валидность подтарифа
        private async Task ValidateSubTariff(SubTariffModel subTariffModel)
        {
            var subTariffCount = await GetSubTariffCountByTariffId(subTariffModel.TariffId);
            //Проверяем корректность значений поля Number
            if (subTariffModel.Number < 1 || subTariffModel.Number > subTariffCount)
                throw new ValidationException("SubTariff", $"Допустимые значения поля Number подтарифа [1 - {subTariffCount}]");
        }

        //Получаем количество подтарифов по Id типа тарифа
        private async Task<int> GetSubTariffCountByTariffId(int tariffId)
        {
            var tariff = (await _tariffRepository
                .GetWithIncludeOnConditionAsync(
                    nameof(TariffExtendedModel.TariffType),
                    t => t.Id == tariffId))
                .FirstOrDefault();
            tariff.EnsureExists(nameof(ErrorMessages.IMS40001));
            return tariff.TariffType.SubTariffCount;
        }

    }
}
