﻿using AutoMapper;

using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Common.Properties;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IMapper _mapper;

        public UserService(IRepository<User> userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }


        public async Task AddUserAsync(UserModel userModel)
        {
            User user = _mapper.Map<User>(userModel);

            var userExist = await _userRepository.AnyAsync(x =>
                x.Login == user.Login &&
                x.Password == user.Password);

            if (userExist)
            {
                throw new ServiceException(nameof(ErrorMessages.IMS50004));
            }

            await _userRepository.AddAsync(user);
        }


        public async Task<UserModel> GetUserByLoginAsync(string login, string password)
        {
            login = login.Trim().ToLower();
            password = password.Trim();
            var user = await _userRepository.GetOnConditionAsync(x => 
                x.Login == login && 
                x.Password == password);
            
            user.EnsureNotEmpty(nameof(ErrorMessages.IMS50003));

            var userModel = user.Select(x => new UserModel()
            {
                Id = x.Id,
                UserName = x.UserName,
                Login = x.Login,
                Password = x.Password,
                Deleted = x.Deleted,
                Email = x.Email,
                PhoneNumber = x.PhoneNumber,
                GetEmailNotification = x.GetEmailNotification,
                GetSmsNotification = x.GetSmsNotification
            }).Single();

            return userModel; 
        }

        
        public async Task DeleteUserAsync(int userId)
        {
            var user = await _userRepository.GetAsync(userId);

            user.EnsureExists(nameof(ErrorMessages.IMS50001));

            if (user.Deleted)
            {
                throw new ServiceException(nameof(ErrorMessages.IMS50002));
            }

            user.Deleted = true;

            await _userRepository.UpdateAsync(user);
        }


        public async Task<UserModel> GetByIdAsync(int userId)
        {
            var user = await _userRepository.GetAsync(userId);

            user.EnsureExists(nameof(ErrorMessages.IMS50001));

            if (user.Deleted)
            {
                throw new ServiceException(nameof(ErrorMessages.IMS50002));
            }

            UserModel userModel = _mapper.Map<UserModel>(user);

            return userModel;
        }


        public async Task UpdateUserAsync(UserModel userModel)
        {
            // Проверяем наличие пользователя в БД
            var user = await _userRepository.GetAsync(userModel.Id);

            user.EnsureExists(nameof(ErrorMessages.IMS50001));

            user.UserName = userModel.UserName;
            user.Login = userModel.Login;
            user.Password = userModel.Password;
            user.Email = userModel.Email;
            user.PhoneNumber = userModel.PhoneNumber;
            user.GetEmailNotification = userModel.GetEmailNotification;
            user.GetSmsNotification = userModel.GetSmsNotification;

            await _userRepository.UpdateAsync(user);
        }
    }
}
