﻿using AutoMapper;

using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Предоставляет методы для работы с аутентификацией пользователя.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : Controller  
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Конструктор контроллера аутентификации пользователя. 
        /// </summary>
        public AccountController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }


        /// <summary>
        /// Проверка корректности введенных логина и пароля.
        /// </summary>
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginModel loginModel)
        {
            //  Проверяем входящие данные
            UserModel userModel = await _userService.GetUserByLoginAsync(loginModel.Login, loginModel.Password);

            // Аутентификация
            await Authenticate(loginModel.Login);
            return Ok(userModel); 
        }


        /// <summary>
        /// Регистрация нового пользователя.
        /// </summary>
        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterModel registerModel)
        {
            UserModel userModel = _mapper.Map<UserModel>(registerModel);

            // Добавляем пользователя в БД
            await _userService.AddUserAsync(userModel);

            // Аутентификация
            await Authenticate(registerModel.Login);
            return Ok(userModel);  
        }


        /// <summary>
        /// Аутентификация.
        /// </summary>
        private async Task Authenticate(string login)
        {
            // Создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, login)
            };

            // Создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            
            // Установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }


        /// <summary>
        /// Выход.
        /// </summary>
        [Authorize]
        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Ok();
        }
    }
}