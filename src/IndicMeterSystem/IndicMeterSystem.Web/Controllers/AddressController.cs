﻿using System.Collections.Generic;
using System.Threading.Tasks;

using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Предоставляет методы для работы с адресами пользователя.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AddressController : ControllerBase
    {
        private readonly IAddressService _addressService;

        /// <summary>
        /// Конструктор контроллера адресов.
        /// </summary>
        /// <param name="addressService">Объект интерфейса по работе с адресами.</param>
        public AddressController(IAddressService addressService)
        {
            _addressService = addressService;
        }

        /// <summary>
        /// Получает адреса по id пользователя.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <returns>Возвращает список адресов пользователя</returns>
        [HttpGet("user/{userId}")]
        public async Task<IEnumerable<AddressModel>> GetAddress(int userId)
        {
            var addresses = await _addressService.GetByUserIdAsync(userId);
            return addresses;
        }

        /// <summary>
        /// Добавляет новый адрес пользователя.
        /// </summary>
        /// <param name="address">Модель данных адреса.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddAddress(AddressModel address)
        {
            await _addressService.AddAddressAsync(address);
            return Ok();
        }

        /// <summary>
        /// Переводит адрес в архив.
        /// </summary>
        /// <param name="addressId">Идентификатор адреса.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        [HttpDelete("delete/{addressId}")]
        public async Task<IActionResult> DeleteAddress(int addressId)
        {
            await _addressService.DeleteAddressAsync(addressId);
            return Ok();
        }

        /// <summary>
        ///Добавляет в базу копию адреса с его счетчиками.
        /// </summary>
        /// <param name="addressId">Идентификатор адреса.</param>
        /// <returns>Возвращает модель нового адреса.</returns>
        [HttpPost("copy/{addressId}")]
        public async Task<InclusiveAddressModel> AddCopy(int addressId)
        {
            var address = await _addressService.AddCopyAsync(addressId);
            return address;
        }


        /// <summary>
        /// Изменяет адрес.
        /// </summary>
        /// <param name="address">Изменяемая модель адреса.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        [HttpPut]
        public async Task<IActionResult> UpdateAddress(AddressModel address)
        {
            await _addressService.UpdateAddressAsync(address);
            return Ok();
        }
    }
}
