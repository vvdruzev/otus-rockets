﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IndicMeterSystem.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class MeterController : ControllerBase
    {
        private readonly IMeterService _meterService;
        private readonly ILogger<MeterController> _logger;

        public MeterController(IMeterService meterService, ILogger<MeterController> logger)
        {
            _meterService = meterService;
            _logger = logger;
        }

        /// <summary>
        /// Метод получает счетчики по id адреса.
        /// </summary>
        /// <param name="addressId">Идентификатор адреса.</param>
        /// <returns>Возвращает список счетчиков</returns>
        [HttpGet("address/{addressId}")]
        public async Task<IEnumerable<MeterModel>> GetMeter(int addressId)
        {
            var meters = await _meterService.GetByAddressIdAsync(addressId);
            _logger.LogTrace(meters.ToList().ToString());
            return meters;
        }

        /// <summary>
        /// Добавление счетчика
        /// </summary>
        /// <param name="meter"></param>
        /// <returns>status</returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddMeter(MeterModel meter)
        {
            await _meterService.AddMeterAsync(meter);
            return Ok();
        }

        /// <summary>
        /// Получает копию счетчика.
        /// </summary>
        /// <param name="meterId">Идентификатор счетчика.</param>
        /// <returns>Возвращает копию счетчика.</returns>
        [HttpGet("copy/{meterId}")]
        public async Task<MeterModel> GetCopyMeter(int meterId)
        {
            var meter = await _meterService.GetCopyAsync(meterId);
            
            return meter;
        }

        /// <summary>
        /// Изменяет существующий счетчик
        /// </summary>
        /// <param name="meter">измененный счетчик</param>
        /// <returns>Статус кода завершения</returns>
        [HttpPut("update")]
        public async Task<IActionResult> UpdateMeter(MeterModel meter)
        {
            await _meterService.UpdateMeterAsync(meter);
            return Ok();
        }

        /// <summary>
        /// Помечает существующий счетчик как закрытый
        /// </summary>
        /// <param name="meterId">измененный счетчик</param>
        /// <returns>Статус кода завершения</returns>
        [HttpDelete("delete/{meterId}")]
        public async Task<IActionResult> DeleteMeter(int meterId)
        {
          await _meterService.DeleteMeterAsync(meterId);
          return Ok();
        }

  }
}
