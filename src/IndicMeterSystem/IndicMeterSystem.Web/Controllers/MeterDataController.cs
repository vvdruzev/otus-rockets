﻿using System;
using System.Threading.Tasks;

using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Предоставляет методы для работы с показаниями.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class MeterDataController : ControllerBase
    {
        private readonly IMeterDataService _meterDataService;

        /// <summary>
        /// Конструктор контроллера показаний.
        /// </summary>
        /// <param name="meterDataService">Объект интерфейса по работе с показаниями.</param>
        public MeterDataController(IMeterDataService meterDataService)
        {
            _meterDataService = meterDataService;
        }

        /// <summary>
        /// Добавляет новую запись показания.
        /// </summary>
        /// <param name="meterDataModel">Модель данных показания.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddMeterData(MeterDataModel meterDataModel)
        {
            var addMeterDataModel = await _meterDataService.AddAsync(meterDataModel);

            return Ok(addMeterDataModel);
        }

        /// <summary>
        /// Получает данные по показанию с данными по платежам.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возсращает модель показания со списком платежей.</returns>
        [HttpGet("{meterDataId}")]
        public async Task<IActionResult> GetById(int meterDataId)
        {
            var meterDataModel = await _meterDataService.GetByIdWithPaymentsAsync(meterDataId);

            return Ok(meterDataModel);
        }

        /// <summary>
        /// Получает данные по показаниям за интервал времени.
        /// </summary>
        /// <param name="dateFrom">Начальная дата интервала.</param>
        /// <param name="dateTo">Конечная дата интервала.</param>
        /// <param name="meterId">Идентификатор счетчика, может быть не задан.</param>
        /// <returns>Возвращает список моделей показаний.</returns>
        [HttpGet("{dateFrom}&{dateTo}&{meterId}")]
        public async Task<IActionResult> GetByInterval(DateTime dateFrom, DateTime dateTo, int meterId = 0)
        {
            var meterDataModels = await _meterDataService
                .GetByIntervalAsync(dateFrom, dateTo, meterId);

            return Ok(meterDataModels);
        }
    }
}
