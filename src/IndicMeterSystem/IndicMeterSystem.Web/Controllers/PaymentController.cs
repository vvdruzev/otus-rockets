﻿using System.Collections.Generic;
using System.Threading.Tasks;

using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Предоставляет методы для работы с платежами.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentService _paymentService;

        /// <summary>
        /// Конструктор контроллера платежей.
        /// </summary>
        /// <param name="paymentService">Объект интерфейса по работе с платежами.</param>
        public PaymentController(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        /// <summary>
        /// Добавляет новую запись платежа.
        /// </summary>
        /// <param name="paymentModel">Модель данных платежа.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        [HttpPost("add")]
        public async Task<IActionResult> Add(PaymentModel paymentModel)
        {
            var addPaymentModel = await _paymentService.AddAsync(paymentModel);

            return Ok(addPaymentModel);
        }

        /// <summary>
        /// Получает список платежей по идентификатору показания.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает список платежей.</returns>
        [HttpGet("meter/{meterDataId}")]
        public async Task<IActionResult> Get(int meterDataId)
        {
            var payment = await _paymentService.GetByMeterDataIdAsync(meterDataId);
            
            return Ok(payment);
        }

        /// <summary>
        /// Получает оплаченную сумму по показанию счетчика.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает оплаченную сумму.</returns>
        [HttpGet("paidsum/{meterDataId}")]
        public async Task<IActionResult> GetPaidlSum(int meterDataId)
        {
            var sum = await _paymentService.GetPaidSumAsync(meterDataId);

            return Ok(sum);
        }

        /// <summary>
        /// Получает общую сумму оплаты показания счетчика.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает общую сумму.</returns>
        [HttpGet("totalsum/{meterDataId}")]
        public async Task<IActionResult> GetTotalSum(int meterDataId)
        {
            var sum = await _paymentService.GetTotalSumAsync(meterDataId);

            return Ok(sum);
        }
    }
}
