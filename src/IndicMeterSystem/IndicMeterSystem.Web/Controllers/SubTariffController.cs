﻿using Microsoft.AspNetCore.Mvc;

using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Методы для работы с подтарифами
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SubTariffController : ControllerBase
    {
        private readonly ISubTariffService _subTariffService;

        /// <summary>
        /// Конструктор контроллера подтарифов
        /// </summary>
        /// <param name="subTariffService"></param>
        public SubTariffController(ISubTariffService subTariffService)
        {
            _subTariffService = subTariffService;
        }

        /// <summary>
        /// Получает список подтарифов по Id тарифа
        /// </summary>
        /// <param name="tariffId">Id тарифа</param>
        /// <returns>Список подтарифов</returns>
        [HttpGet("tariff/{tariffId}")]
        public async Task<IActionResult> GetSubTariff(int tariffId)
        {
            var subTariffs = await _subTariffService.GetByTariffIdAsync(tariffId);
            return Ok(subTariffs);
        }

        /// <summary>
        /// Добавляет новый подтариф
        /// </summary>
        /// <param name="subTariff">Модель подтарифа</param>
        /// <returns>Статус кода завершения</returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddSubTariff(SubTariffModel subTariff)
        {
            await _subTariffService.AddAsync(subTariff);
            return Ok();
        }

    }
}
