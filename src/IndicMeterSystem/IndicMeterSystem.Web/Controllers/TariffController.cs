﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using Microsoft.AspNetCore.Authorization;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Методы для работы с тарифами
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TariffController : ControllerBase
    {
        private readonly ITariffService _tariffService;

        /// <summary>
        /// Конструктор контроллера тарифов
        /// </summary>
        public TariffController(ITariffService tariffService)
            => _tariffService = tariffService;

        /// <summary>
        /// Получает тариф по Id
        /// </summary>
        /// <returns>Модель тарифа</returns>
        [HttpGet("{tariffId}")]
        public async Task<IActionResult> GetTariff(int tariffId)
        {
            var tariff = await _tariffService.GetByIdAsync(tariffId);
            return Ok(tariff);
        }

        /// <summary>
        /// Добавляет новый тариф
        /// </summary>
        /// <param name="tariff">Модель тарифа</param>
        /// <returns>Статус кода завершения</returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddTariff(TariffExtendedModel tariff)
        {
            await _tariffService.AddAsync(tariff);
            return Ok();
        }

        /// <summary>
        /// Переводит тариф в архив.
        /// </summary>
        /// <param name="tariffId">Id тарифа</param>
        /// <returns>Статус кода завершения</returns>
        [HttpDelete("{tariffId}")]
        public async Task<IActionResult> DeleteTariff(int tariffId)
        {
            await _tariffService.DeleteAsync(tariffId);
            return Ok();
        }

        /// <summary>
        /// Изменяет существующий тариф
        /// </summary>
        /// <param name="tariff">измененный тариф</param>
        /// <returns>Статус кода завершения</returns>
        [HttpPut]
        public async Task<IActionResult> UpdateTariff(TariffExtendedModel tariff)
        {
            await _tariffService.UpdateAsync(tariff);
            return Ok();
        }
    }
}
