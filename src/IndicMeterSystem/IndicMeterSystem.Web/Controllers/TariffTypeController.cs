﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using Microsoft.AspNetCore.Authorization;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Методы для работы с тпами тарифа
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TariffTypeController : ControllerBase
    {
        private readonly ITariffTypeService _tariffTypeService;

        public TariffTypeController(ITariffTypeService service)
        {
            _tariffTypeService = service;
        }

        /// <summary>
        /// Получает список всех типов тарифов
        /// </summary>
        /// <returns>Список типов тарифов</returns>
        [HttpGet()]
        public async Task<IEnumerable<TariffTypeModel>> GetAll()
        {
            return await _tariffTypeService.GetAllAsync();
        }

        /// <summary>
        /// Получает тип тарифа по Id
        /// </summary>
        /// <returns>Тип тарифа</returns>
        [HttpGet("{tariffTypeId}")]
        public async Task<TariffTypeModel> GetTariffType(int tariffTypeId)
        {
            return await _tariffTypeService.GetByIdAsync(tariffTypeId);
        }

    }
}
