﻿using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Предоставляет методы для работы с пользователями.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Конструктор контроллера пользователей.
        /// </summary>
        /// <param name="userService">Объект интерфейса по работе с пользователями.</param>
        public UserController(IUserService userService)
        {
            _userService = userService;
        }


        /// <summary>
        /// Переводит пользователя в архив.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        [HttpDelete("delete/{userId}")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            await _userService.DeleteUserAsync(userId);
            return Ok();
        }


        /// <summary>
        /// Получает данные пользователя по Id.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <returns>Модель пользователя.</returns>
        [HttpGet("{userId}")]
        public async Task<UserModel> GetUser(int userId)
        {
            var user = await _userService.GetByIdAsync(userId);
            return user;
        }


        /// <summary>
        /// Изменяет данные пользователя.
        /// </summary>
        /// <param name="user">Изменяемая модель пользователя.</param>
        /// <returns>Статус кода завершения.</returns>
        [HttpPut]
        public async Task<IActionResult> UpdateUser(UserModel user)
        {
            await _userService.UpdateUserAsync(user);
            return Ok();
        }
    }
}
