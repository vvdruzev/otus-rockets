﻿using System.Net;

using Microsoft.AspNetCore.Mvc;

namespace IndicMeterSystem.Web.Infrastructure.Extensions
{
	/// <summary>
	/// Расширения дял работы с <see cref="ProblemDetails"/>.
	/// </summary>
	public static class ProblemDetailsExtensions
	{
		/// <summary>
		/// MIME-type для описания проблемы в ответе.
		/// </summary>
		public static string MimeType => "application/problem+json";

		/// <summary>
		/// Добавляет пару ключ-значение в описание проблемы.
		/// </summary>
		/// <param name="problemDetails">Описание проблемы.</param>
		/// <param name="key">Ключ.</param>
		/// <param name="value">Значение для ключа.</param>
		/// <returns>Описание проблемы с добавленной парой.</returns>
		public static ProblemDetails AddOrUpdateCustomValue(
			this ProblemDetails problemDetails,
			string key,
			object value)
		{
			problemDetails.Extensions[key] = value;

			return problemDetails;
		}

		/// <summary>
		/// Устанавливает статус-код для описания ошибки.
		/// </summary>
		/// <param name="problemDetails">Описание проблемы.</param>
		/// <param name="statusCode">HTTP статус код.</param>
		/// <returns>Описание с необходимым статус-кодом.</returns>
		public static ProblemDetails WithStatusCode(
			 this ProblemDetails problemDetails,
			HttpStatusCode statusCode)
		{
			problemDetails.Status = (int)statusCode;

			return problemDetails;
		}

		/// <summary>
		/// Добавляет код ошибки в описание проблемы.
		/// </summary>
		/// <param name="problemDetails">Описание проблемы.</param>
		/// <param name="errorCode">Код ошибки.</param>
		/// <returns>Описание проблемы с кодом ошибки.</returns>
		public static ProblemDetails WithErrorCode(
			this ProblemDetails problemDetails,
			string errorCode)
		{
			return problemDetails.AddOrUpdateCustomValue("errorCode", errorCode);
		}

		/// <summary>
		/// Добавляет сообщение об ошибке.
		/// </summary>
		/// <param name="problemDetails">Описание ошибки.</param>
		/// <param name="errorMessage">Сообщение об ошибке.</param>
		/// <returns>Описание с сообщением.</returns>
		public static ProblemDetails WithErrorMessage(
			this ProblemDetails problemDetails,
			string errorMessage)
		{
			problemDetails.Title = errorMessage;

			return problemDetails;
		}


		/// <summary>
		/// Добавляет стек вызовов.
		/// </summary>
		/// <param name="problemDetails">Описание ошибки.</param>
		/// <param name="stackTrace">Стек вызовов.</param>
		/// <returns>Описание ошибки со стеком вызовов.</returns>
		public static ProblemDetails WithStackTrace(
			this ProblemDetails problemDetails,
			string stackTrace)
		{
			return problemDetails.AddOrUpdateCustomValue("stackTrace", stackTrace);
		}

		/// <summary>
		/// Добавляет дополнительные сведения об ошибке.
		/// </summary>
		/// <param name="problemDetails">Описание ошибки.</param>
		/// <param name="details">Дополнительные сведения.</param>
		/// <returns> Дополненное описание проблемы. </returns>
		public static ProblemDetails WithErrorDetails(
			this ProblemDetails problemDetails,
			object details)
		{
			return problemDetails.AddOrUpdateCustomValue("details", details);
		}
	}
}
