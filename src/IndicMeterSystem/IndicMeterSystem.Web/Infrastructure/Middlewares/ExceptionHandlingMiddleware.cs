﻿using System;
using System.Text.Json;
using System.Net;
using System.Threading.Tasks;

using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Common.Properties;
using IndicMeterSystem.Web.Infrastructure.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace IndicMeterSystem.Web.Infrastructure.Middlewares
{
	/// <summary>
	/// Обработка ошибок.
	/// </summary>
	public class ExceptionHandlingMiddleware
	{
		/// <summary>
		/// Request delegate.
		/// </summary>
		private readonly RequestDelegate _next;

		/// <summary>
		/// Параметры окружения.
		/// </summary>
		private readonly IHostEnvironment _env;

		private readonly ILogger<ExceptionHandlingMiddleware> _logger;

		/// <summary>
		/// Создает экземпляр класса <see cref="ExceptionHandlingMiddleware"/>.
		/// </summary>
		/// <param name="next">Делегат вызова следующего обработчика.</param>
		/// <param name="env">Параметры окружения.</param>
		public ExceptionHandlingMiddleware(
			RequestDelegate next, 
			IHostEnvironment env,
			ILogger<ExceptionHandlingMiddleware> logger)
		{
			_next = next;
			_env = env;
			_logger = logger;
		}

		/// <summary>
		/// Вызывает обработчик.
		/// </summary>
		/// <param name="context">Http context.</param>
		/// <returns>Operation task.</returns>
		public async Task InvokeAsync(HttpContext context)
		{
			try
			{
				await _next(context);
			}
			catch (Exception ex)
			{
                if (ex is ValidationException exception)
                {
					_logger.LogError($"Возникло исключение " +
						$"\"{exception.Details.ValidationMessage}\". " +
						$"Объект исключения \"{exception.Details.PropertyName}\"");
				}
                else
                {
					_logger.LogError($"Возникло исключение \"{ex.Message}\".");
				}

				var response = CreateProblemDetails(ex);

				context.Response.ContentType = ProblemDetailsExtensions.MimeType;
				if (response.Status != null) 
					context.Response.StatusCode = response.Status.Value;

				var settings = new JsonSerializerOptions
				{
					PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
				};

				await context.Response.WriteAsync(JsonSerializer.Serialize(response, settings));
			}
		}

		/// <summary>
		/// Создает описание проблемы с данными из исключения.
		/// </summary>
		/// <param name="exception">Исключение.</param>
		/// <returns>Описание проблемы.</returns>
		private ProblemDetails CreateProblemDetails(Exception exception)
		{
			var problemDetails = CreateDefaultProblemDetails(exception);

			return exception switch
			{
				/* любые другие исключения, для которых нужна особенная обработка */
				ValidationException validationException => problemDetails.WithException(validationException),
				AppException appException => problemDetails.WithException(appException),
				_ => problemDetails
			};
		}

		/// <summary>
		/// Создает описание проблемы по умолчанию.
		/// </summary>
		/// <param name="exception"></param>
		/// <returns></returns>
		private ProblemDetails CreateDefaultProblemDetails(Exception exception)
		{
			var problemDetails = new ProblemDetails();

			if (exception.InnerException != null)
			{
				problemDetails = problemDetails.WithErrorDetails(
					new
					{
						exception.InnerException.Message,
					});
			}

			problemDetails
				.WithStatusCode(HttpStatusCode.InternalServerError)
				.WithErrorCode(nameof(ErrorMessages.IMS10001))
				.WithErrorMessage(ErrorMessages.IMS10001);

			if (_env.IsDevelopment())
				problemDetails.WithStackTrace(exception.StackTrace);

			return problemDetails;
		}
	}
}
