﻿using IndicMeterSystem.Web.LoggerMiddlewear;
using Microsoft.AspNetCore.Builder;

namespace IndicMeterSystem.Web.Infrastructure.Middlewares
{
    public static class RequestResponseLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestResponseLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestResponseLoggingMiddleware>();
        }
    }
}
