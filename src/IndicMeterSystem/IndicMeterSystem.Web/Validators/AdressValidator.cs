﻿using FluentValidation;
using IndicMeterSystem.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Validators
{
    /// <summary>
    /// Валидатор для <see cref="AddressModel"/>.<br/>
    /// </summary>

    public class AdressValidator: AbstractValidator<AddressModel>
    {
        /// <inheritdoc />
        public AdressValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull();
            RuleFor(x => x.UserId).NotEmpty().NotNull();
        }
    }
}
