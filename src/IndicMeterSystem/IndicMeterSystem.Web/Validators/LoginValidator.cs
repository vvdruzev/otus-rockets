﻿using FluentValidation;
using IndicMeterSystem.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Validators
{
    /// <summary>
    /// Валидатор для <see cref="LoginModel"/>.<br/>
    /// </summary>

    public class LoginValidator : AbstractValidator<LoginModel>
    {
        public LoginValidator()
        {
            RuleFor(x => x.Login).NotEmpty().WithMessage("Не указан логин");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Не указан пароль");
        }
    }
}
