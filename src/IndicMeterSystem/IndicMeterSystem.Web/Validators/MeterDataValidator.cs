﻿using FluentValidation;
using IndicMeterSystem.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Validators
{
    /// <summary>
    /// Валидатор для <see cref="MeterDataModel"/>.<br/>
    /// </summary>

    public class MeterDataValidator : AbstractValidator<MeterDataModel>
    {
        /// <inheritdoc />
        public MeterDataValidator()
        {
            RuleFor(x => x.MeterId).NotEmpty().NotNull().WithMessage($"Показания должны быть привязаны к счетчику") ;

            // Для каждого члена колекции MeterValues идёт проверка < 1
            RuleForEach(x => x.MeterValues).NotNull().ChildRules(values =>
            {
                values.RuleFor(x => x.Value).GreaterThanOrEqualTo(1).WithMessage("Показания не могут быть меньше единицы");
            });
        }
    }
}
