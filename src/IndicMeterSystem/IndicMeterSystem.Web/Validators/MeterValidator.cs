﻿using FluentValidation;
using FluentValidation.Results;
using IndicMeterSystem.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Validators
{
    /// <summary>
    /// Валидатор для <see cref="MeterModel"/>.<br/>
    /// </summary>
    public class MeterValidator : AbstractValidator<MeterModel>
    {
        /// <inheritdoc />
        public MeterValidator()
        {
            RuleFor(x => x.AddressId).NotEmpty().NotNull();
            RuleFor(x => x.SerialNumber).NotEmpty().NotNull();
            RuleFor(x => x.TariffId).NotEmpty().NotNull(); 
            RuleFor(x => x.DateFrom)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .NotNull()
                .Must(DateForIsOldValid).WithMessage("Некорректное поле {PropertyName}. Слишком старый счетчик")
                .Must(DateForCurrentValid).WithMessage("Некорректное поле {PropertyName}. Счетчик не может быть установлен будущей датой");
        }

        private bool DateForCurrentValid(DateTime arg)
        {
            if (arg >= DateTime.Now)
            {
                return false;
            }

            return true;
        }

        private bool DateForIsOldValid(DateTime arg)
        {
            int currentYear = DateTime.Now.Year;
            int dobYear = arg.Year;

            // не старше 10 лет
            if (dobYear >= currentYear - 10)
            {
                return true;
            }

            return false;
        }

        public override ValidationResult Validate(ValidationContext<MeterModel> instance)
        {
            return instance == null
                ? new ValidationResult(new[] { new ValidationFailure("Meter", "Meter cannot be null") })
                : base.Validate(instance);
        }

    }
}
