﻿using FluentValidation;
using IndicMeterSystem.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Validators
{
    /// <summary>
    /// Валидатор для <see cref="UserModel"/>.<br/>
    /// </summary>
    public class UserValidator : AbstractValidator<UserModel>
    {
        public UserValidator()
        {
            RuleFor(x => x.UserName).NotEmpty();
            RuleFor(x => x.Login).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();

            //поле PhoneNumber проверяется если установлен флаг GetSmsNotification 
            RuleFor(x => x.PhoneNumber).Matches("^((8|\\+7)[\\-]?)?(\\(?\\d{3}\\)?[\\-]?)?[\\d\\-]{7,10}$").WithMessage("{PropertyName} имеет неверный формат +7xxxxxxxxxx").When(x => x.GetSmsNotification);

            //поле Email проверяется если установлен флаг GetEmailNotification 
            RuleFor(x => x.Email).EmailAddress().When(x => x.GetEmailNotification);

        }
    }
}
