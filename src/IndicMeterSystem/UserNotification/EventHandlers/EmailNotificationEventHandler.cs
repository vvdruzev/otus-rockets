﻿using BusinesEvent;

using Infrastructure.EventBus.EventBusInfrastructure.Abstraction;

using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

using System;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Net;

namespace UserNotification
{
    public class EmailNotificationEventHandler : 
        IIntegrationEventHandler<UserEmailNotifEvent>
    {
        private readonly SmptConfig _smptconfig;
        private readonly ILogger<EmailNotificationEventHandler> _logger;

        public EmailNotificationEventHandler(
            IOptions<SmptConfig> config,
            ILogger<EmailNotificationEventHandler> logger)
        {
            _smptconfig = config.Value;
            _logger = logger;
        }

        public async Task Handle(UserEmailNotifEvent @event)
        {
            if (DateTime.Today < @event.EventDateTo)
            {
                await SendEmailAsync(@event.Email, @event.Title, @event.Body);
            }
        }

        /// <summary>
        /// Отправляет email сообщения.
        /// </summary>
        /// <param name="email">Электронная почта получателя.</param>
        /// <param name="subject">Тема сообщения.</param>
        /// <param name="message">Текст сообщения.</param>
        /// <returns></returns>
        private async Task SendEmailAsync(string email, string subject, string message)
        {
            try
            {
#if RELEASE
                var emailMessage = new MailMessage(
                    new MailAddress(_smptconfig.SenderEmail, "Учет показаний ЖКХ"),
                    new MailAddress(email))
                {
                    Subject = subject,
                    Body = message
                };

                using var client = new SmtpClient(_smptconfig.Server, _smptconfig.Port);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = _smptconfig.SslEnabled;
                client.Credentials = new NetworkCredential(
                    _smptconfig.SenderEmail, _smptconfig.SenderPass);
                await client.SendMailAsync(emailMessage);
#endif
                _logger.LogDebug("\nОтправлено оповещение по email: "
                    + $" email получателя '{email}';"
                    + $" тема '{subject}';"
                    + $" текст '{message}'.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Ошибка отправки email пользователю {email}.");
            }
        }
    }
}
