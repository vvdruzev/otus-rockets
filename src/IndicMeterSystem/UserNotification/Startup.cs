using BusinesEvent;
using Infrastructure.EventBus.EventBusInfrastructure.Abstraction;
using Infrastructure.EventBus.EventBusRabbitMQ;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace UserNotification
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<SmptConfig>(Configuration.GetSection("SmtpConfig"));
            
            services.AddEventBus(this.Configuration);
            services.AddTransient<EmailNotificationEventHandler>();
            services.AddTransient<SmsNotificationEventHandler>();

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ConfigureEventBus(app);

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Start user notification.");
                });
            });
        }

        protected virtual void ConfigureEventBus(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

            eventBus.Subscribe<UserMeterDataEmailNotifEvent, EmailNotificationEventHandler>();
            eventBus.Subscribe<UserPaymentEmailNotifEvent, EmailNotificationEventHandler>();

            eventBus.Subscribe<UserMeterDataSmsNotifEvent, SmsNotificationEventHandler>();
            eventBus.Subscribe<UserPaymentSmsNotifEvent, SmsNotificationEventHandler>();

            eventBus.StartConsume();
        }
    }
}
