using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using IndicMeterSystem.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest.Web
{
    public class UnitTestMeterController
    {
        public Mock<IMeterService> _meterService;
        public Mock<ILogger<MeterController>> _mockLogger;

        [Fact]
        public async void TestGetMeter()
        {

            _meterService = new Mock<IMeterService>();
            var model = new MeterModel() { Id = 1, AddressId = 1, SerialNumber = 12314, TariffId = 1, Description = "asdas" };
            IEnumerable<MeterModel> metersmodel = new List<MeterModel>() { model };

            _meterService.Setup(x => x.GetByAddressIdAsync(It.IsAny<int>())).ReturnsAsync(metersmodel);

            _mockLogger = new Mock<ILogger<MeterController>>();

            var controller = new MeterController(_meterService.Object, _mockLogger.Object);
            var response = await controller.GetMeter(1);

            Assert.Equal(response.Select(x => x.AddressId).First(), model.AddressId);

        }

        [Fact]
        public async void TestAddMeter()
        {
            _meterService = new Mock<IMeterService>();
            _mockLogger = new Mock<ILogger<MeterController>>();

            _meterService.Setup(x => x.AddMeterAsync(It.IsAny<MeterModel>()));
            var controller = new MeterController(_meterService.Object, _mockLogger.Object);

            var model = new MeterModel() { Id = 1, AddressId = 1, SerialNumber = 12314, TariffId = 1, Description = "asdas" };

            var response = await controller.AddMeter(model);

            var contentResult = response as OkResult;

            Assert.Equal(200, contentResult.StatusCode);
        }
    }
}
